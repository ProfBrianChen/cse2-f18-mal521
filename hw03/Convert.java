// Michaela Lewis 
// hw #3 pt 1
//this program asks the user for doubles that represent the # of acres
//of land affected by hurricane precipation and how many inches of rain 
//were dropped on average and converts that into cubic miles 

import java.util.Scanner;

public class Convert{
  public static void main(String[]args){
    //gather info
    Scanner myScanner = new Scanner (System.in); //scanner instance created 
    System.out.print("Enter the affected area in acres:  ");
    double affAcre = myScanner.nextDouble();
    
    //0.0015625
    
    System.out.print("Enter the rainfall in the affected area:   ");
    double affRainFall = myScanner.nextDouble();
    
    //1.57828e-5
    affAcre *= 0.0015625;
    affRainFall *= 1.57828e-5;
    //calculations
    double cubicMiles = affAcre * affRainFall;
   
    /*
     gallons = 1/ (affAcre * affRainFall) * 27154; 
     cubicMiles =  (int) (1/ (gallons) * 946263747755) ;
    */
    
     System.out.println( cubicMiles + " cubic miles");
    
  }
}