//Michaela Lewis 
//lab #3
//this program uses the scnner class to get the original cost, 
//percentage tip, and the number of ways to spit a check on a dinner with friends

import java.util.Scanner;

public class Check{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); //instance of scanner 
    System.out.print("Enter the orginal cost of the check in the form xx.xx: "); //tell user to enter the cost
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx): ");
    double tipPercent =  myScanner.nextDouble();
    
    System.out.print("Enter the number of people who went out to dinner: ");
    double numPeople = myScanner.nextDouble();
    tipPercent /= 100;
    
   //output the amount each member needs to spend in order to pay the check
  double costPerPerson;
  int dollars;//whole dollar amount of cost 
  int dimes;
  int pennies; //for storing digits
  
  double totalCost = checkCost * (1 + tipPercent);
  costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
 dollars = (int) (costPerPerson);
 dimes = (int)(costPerPerson * 10) % 10;
 pennies = (int)(costPerPerson * 100) % 10;
 System.out.println("Each person in the group owes $: " + dollars + "." + dimes + pennies);

  }
}