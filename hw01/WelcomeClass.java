//Michaela Lewis
//Homework 1


public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-m--a--l--5--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I was born in Georgetown, Guyana and moved to the U.S with my family. I'm studying computer engineering and am considering an English minor");
                      
  }
}