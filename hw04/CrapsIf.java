//Michaela Lewis 
//Hw #4 Part 1
//This program plays a dice (random or user input), determines the slang terminology and prints it
import java.util.Scanner;

public class CrapsIf{
  
  public static void main(String[]args){
    //asks the user if they would like to randomly cast a dice
    Scanner userScanner = new Scanner (System.in);
    System.out.println("Would you like to randomly cast dice enter 1 for YES and 2 for NO");
    int responce = userScanner.nextInt();
    
    int dice1;
    int dice2;
    int userDice;
    int userDice2;
    //determine the slang terminology of the outcome of the roll 
    int totalRanDice;//randomly selected dice
    int totalUserInp;//user inputed dice;
    String slangTerm;
    
    
    //if yes -> generate two random dice 
    if (responce == 1)
    {
     dice1 = (int) (Math.random()*6)+1;
     dice2 = (int) (Math.random()*6)+1;
    System.out.println("Your dice are: " + dice1 + " and " + dice2);
      
   if ((dice1 == 1) && (dice2 == 1))
      {
        slangTerm = "Snake Eyes";
        System.out.println("You rolled: " + slangTerm);
      }
      else if ((dice1 == 1) && (dice2 == 2))
      {
        slangTerm = "Ace Deuce";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 1) && (dice2 == 3))
      {
        slangTerm = "Easy Four";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 1) && (dice2 == 4))
      {
        slangTerm = "Fever Five";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 1) && (dice2 == 5))
      {
        slangTerm = "Easy Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 1) && (dice2 == 6))
      {
        slangTerm = "Seven Out";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 2) && (dice2 == 2))
      {
        slangTerm = "Hard Four";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 2) && (dice2 == 3))
      {
        slangTerm = "Fever Five";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((dice1 == 2) && (dice2 == 4))
      {
        slangTerm = "Easy Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 2) && (dice2 == 5))
      {
        slangTerm = "Easy Seven";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 2) && (dice2 == 6))
      {
        slangTerm = "Easy Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 3) && (dice2 == 3))
      {
        slangTerm = "Hard Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 3) && (dice2 == 4))
      {
        slangTerm = "Seven Out";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 3) && (dice2 == 5))
      {
        slangTerm = "Easy Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 3) && (dice2 == 6) || (dice1 == 4) && (dice2 == 5))
      {
        slangTerm = "Nine";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 4) && (dice2 == 4))
      {
        slangTerm = "Hard Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 4) && (dice2 == 6))
      {
        slangTerm = "Easy Ten";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 5) && (dice2 == 5))
      {
        slangTerm = "Hard Ten";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 5) && (dice2 == 6))
      {
        slangTerm = "Yo-leven";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((dice1 == 6) && (dice2 == 6))
      {
        slangTerm = "Boxcars";
        System.out.println("You rolled: " + slangTerm);
      }  
   
    }
    
    //if no  -> ask the user for two integers that represent dice 
    else if (responce == 2)
    {
    Scanner myScanner1 = new Scanner (System.in);
    System.out.println("Please enter a value for your first dice:");
    userDice = myScanner1.nextInt();
    
    Scanner myScanner2 = new Scanner (System.in);
    System.out.println("Please enter a value for your second dice:");
    userDice2 = myScanner2.nextInt();
    
    //check if the dice entered are within range 
    if ((userDice < 1 || userDice > 6) || (userDice2 < 1 || userDice2 > 6))
    {
    System.out.println("ERROR! Please enter a value for dice that is between 1 and 6 {inclusive}");
    }
      //if it is execute and print
      
      if ((userDice == 1) && (userDice2 == 1))
      {
        slangTerm = "Snake Eyes";
        System.out.println("You rolled: " + slangTerm);
      }
      else if ((userDice == 1) && (userDice2 == 2))
      {
        slangTerm = "Ace Deuce";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 1) && (userDice2 == 3))
      {
        slangTerm = "Easy Four";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 1) && (userDice2 == 4))
      {
        slangTerm = "Fever Five";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 1) && (userDice2 == 5))
      {
        slangTerm = "Easy Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 1) && (userDice2 == 6))
      {
        slangTerm = "Seven Out";
        System.out.println("You rolled: " + slangTerm);
      }
     
      else if ((userDice == 2) && (userDice2 == 2))
      {
        slangTerm = "Hard Four";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 2) && (userDice2 == 3))
      {
        slangTerm = "Fever Five";
        System.out.println("You rolled: " + slangTerm);
      }
      
      else if ((userDice == 2) && (userDice2 == 4))
      {
        slangTerm = "Easy Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 2) && (userDice2 == 5))
      {
        slangTerm = "Easy Seven";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 2) && (userDice2 == 6))
      {
        slangTerm = "Easy Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 3) && (userDice2 == 3))
      {
        slangTerm = "Hard Six";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 3) && (userDice2 == 4))
      {
        slangTerm = "Seven Out";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 3) && (userDice2 == 5))
      {
        slangTerm = "Easy Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 3) && (userDice2 == 6) || (userDice == 4) && (userDice2 == 5))
      {
        slangTerm = "Nine";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 4) && (userDice2 == 4))
      {
        slangTerm = "Hard Eight";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 4) && (userDice2 == 6))
      {
        slangTerm = "Easy Ten";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 5) && (userDice2 == 5))
      {
        slangTerm = "Hard Ten";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 5) && (userDice2 == 6))
      {
        slangTerm = "Yo-leven";
        System.out.println("You rolled: " + slangTerm);
      }
      
       else if ((userDice == 6) && (userDice2 == 6))
      {
        slangTerm = "Boxcars";
        System.out.println("You rolled: " + slangTerm);
      }
      
      
    } //end of else statement 
    
  
    }
    

} //class end   