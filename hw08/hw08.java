//Michaela Lewis 
//Homework #8
//This program shuffles a deck of cards, prints out the cards in the deck, all shuffled, then gets a hand of cards 
//and prints them out

import java.util.Scanner;
import java.util.Random;


public class hw08{ 
public static void main(String[] args) { 

Scanner scan = new Scanner(System.in); 
//suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; // there are 52 cards 
String[] hand = new String[5]; //there are 5 cards in your hand 

int numCards; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" ");
} 

System.out.println();
  
//call shuffle method
shuffle(cards); 
//call printArray method
printArray(cards);
  
//ask the user how many cards do thry want to pull 
//put that number in num cards
Scanner myScan = new Scanner(System.in);
System.out.println("how many cards would you like?");
numCards = myScan.nextInt();
getHand(cards, 51, numCards); 
index -= numCards;

}
 


//this method will print out all of the cards 
  public static String[] printArray(String cards[]){
      for(int i = 0; i < 52; i++){
          System.out.print(cards[i]+ " ");
      }
    return cards;

  }
  //this method will pull five cards from the deck
  public static void getHand(String cards[], int index, int numCards){
      System.out.println("Hand: ");
    String[] hand = new String[numCards]; 
      for(int a=0; a < numCards ; a++){
          hand[a] = cards[index];
          index--;
         System.out.print(hand[a] + " ");
      }
    }
  
  //this method will shuffle all the cards in the deck
  public static String[] shuffle(String cards[]){
      //shuffle 
      System.out.println("Shuffled: ");
      Random randInt = new Random();
      for (int y = 0; y < 52; y++){
          int randInt2 = randInt.nextInt(52);
          String thrdCup = cards[y];
          cards[y] = cards[randInt2];
          cards[randInt2] = thrdCup;
      }
    return cards;
      
  }
  


}