//Michaela Lewis 
//Hw #2

public class Arithmetic{
  public static void main(String[]args){
  //variables
    //givens
    int numPants = 3; //# of pairs of pants 
    double pantsPrice = 34.98; //cost per pair of pant
    int numShirts = 2;  //# of sweatershirts
    double shirtPrice = 24.99; //cost per shirt 
    int numBelts = 1; //# of belts
    double beltPrice = 33.99; //cost per belt
    double paSalesTax = 0.06;    //tax rate 
    
    //totals 
    double pricePreTax; //cost of purchase before tax 
    double totalSalesTax;//total sales tax
    double totalPaid; //totalpaid
    double pantsCostPre; //cost of pants before sales tax
    double pantsSalesTax; //sales tax of pants
    double shirtsCostPre; //cost of shirts before sales tax
    double shirtsSalesTax; //sales tax of shirts
    double beltsCostPre; //cost of belts before sales tax
    double beltsSalesTax; //sales tax of belts
  
    //calcs
   pantsCostPre = (pantsPrice) * (numPants);
   pantsSalesTax =  pantsCostPre * paSalesTax * 100;
   int myInt = (int) pantsSalesTax;
   shirtsCostPre = (shirtPrice) * (numShirts);
   shirtsSalesTax = shirtsCostPre * paSalesTax * 100;
   int myInt2 = (int) shirtsSalesTax;
   beltsCostPre = (beltPrice) * (numBelts);
   beltsSalesTax = beltsCostPre * paSalesTax * 100;
   int myInt3 = (int) beltsSalesTax;
   pricePreTax = (pantsCostPre) + (shirtsCostPre) + (beltsCostPre);
   totalSalesTax = (myInt) + (myInt2) + (myInt3);
   int myInt4 = (int) totalSalesTax;
   double variable = pantsCostPre * paSalesTax + shirtsCostPre * paSalesTax + beltsCostPre * paSalesTax;
   totalPaid = (pricePreTax + variable) * 100;
   int myInt5 = (int) totalPaid;
   
    //print
   System.out.println("The total cost of the pants are: $" + pantsCostPre + " and the sales tax is: $" +
                    myInt/100.0);
   System.out.println("The total cost of the shirts are: $" + shirtsCostPre + " and the sales tax is: $" + 
                      myInt2/100.0);
   System.out.println("The total cost of the belts are: $" + beltsCostPre + " and the sales tax is: $" + 
                      myInt3/100.0);
   System.out.println("The total cost before taxes is: $" + pricePreTax);
   System.out.println("The total sales tax is: $" + myInt4/100.0);
   System.out.println("The total paid is: $" + myInt5/100.0);
    
  }
  
}

