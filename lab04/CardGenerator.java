//Michaela Lewis 
//lab #04
//This lab generates a random number and tells what the card is

public class CardGenerator{
  public static void main(String[] args){
    // generate random number
		int card = (int) (Math.random()*51);
	
    //card type
    String cardName = " ";
    
    if (card >= 1 && card <= 13)
    {
      cardName = "Diamonds";    
    }  
    else if (card >=14 && card <= 26)
    {
      cardName = "Clubs";
    }
     else if (card >=27 && card <= 39)
    {
     cardName = "Hearts";
    }
     else if (card >=40 && card <= 52)
     {
     cardName = "Spades";
     }
    
    //card identity
    String highName = "";
    card = card % 13;
      switch (card)
      {
        case 1: highName = "Ace";
        break;
        case 2: highName = "King";
        break;
        case 3: highName = "Queen";
        break;
        case 4: highName = "Jack";
        break;
        default: highName = String.valueOf(cardName);
        break;
      }
   //print card choosen
   //aces
    if (card <= 1)
    {
    System.out.println("You picked the Ace of " + cardName);
    }
   //regular degular
    else if (card >= 1 && card <= 10)
    {
    System.out.println("You picked the " + card + " of " + cardName);
    }
  //higher order
   else
   {
      System.out.println("You picked the " + card + " of " + highName);
   }

}
}
