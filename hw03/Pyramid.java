//Michaela Lewis
//hw #3 pt 2
//this program gets the dimensions
//of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;

public class Pyramid {
  public static void main (String [] args){
    
    //get length of pyramid base from uset
    Scanner myScanner = new Scanner (System.in);
    System.out.print("The square side of the pyramid is (input length): ");
    int length = myScanner.nextInt();
    
    //get height of pyramid
    System.out.print("The height of the pyramid is (input height): ");
    int height = myScanner.nextInt();
    
    //calculate
    int volume;
    volume = ((length)* (length)* (height/3));
    
    //output
    System.out.println("The volume inside the pyramid is: " + volume);
    
  }
}