//Michaela Lewis
//Homework #7
//this program manipulates and entered string

import java.util.Scanner; 
public class hw07{

public static String sampleText(){
    // prompts the user to enter a string
    Scanner myScanner = new Scanner(System.in);
    System.out.println("enter a string: ");
    String userString = myScanner.nextLine();
    System.out.println("You entered:"+ userString);
    return userString;
}

public static char printMenu(){

System.out.println("please select an option from the MENU");
System.out.println("c - number of non-whitespace characters");
System.out.println("w - number of words");
System.out.println("f - find text");
System.out.println("r - replace all !'s");
System.out.println("s - shorten spaces");
System.out.println("q - quit");
System.out.println("s - shorten spaces");
System.out.println("");
System.out.println("choose an option: ");

String userChar = myScanner.next().charAt(0);
return userChar;
}


public static int getNumOfNonWSCharacters(String userString){
    String senWithNonWhits = sampleText();

//count the number characters 
    int count = 0;
 for(int i = 0; i <= str.length()-1; i++) {
     senWithNonWhits.isWhitespace(str.charAt(i));
     count++;
     }
return count;
}


public static int getNumOfWords(String sampleText){
 if(sampleText == null || sampleText.isEmpty());
    return 0;
}
String[]words = sampleText.split("\\s+");
return words.length;
}


public static int findText(String textToFind, String sampleText){
    int counter = 0;
    int Index = 0;
    Index = sampleText.indexOf(textToFind, Index);
    if(Index != -1){
    counter++;
    Index += textToFind.length();
    }
    return counter; //number of times the index values are foun
    }

    
    public static String replaceExclamation(String sampleText){
    String Text = sampleText.replace('!', '.');
    return Text; //returns the sample text with the changes
    }

    // shortenSpace method
    public static String ace(String sampleText){
    // replaces sets of spaces of more than 1 space with a single space
    String fixSpace = sampleText.trim().replaceAll(" +", " ");
    return fixSpace; // returns the sample text with the changes
    }
public static void main(String[]args){
Scanner myScanner = new Scanner(System.in); 
    int placeHolder = 0; // place holder
    String st = sampleText(); // call sampleText  
    char printer = printMenu(); // call printMenu

    while (placeHolder == 0){
    if (printer == 'q'){ //quit
    break;
    }
    else if (printer == 'c'){ //count the non-whitespace characte
    
    int nonWSChar = getNumOfNonWSCharacters(st); // uses method to get number of non-whitespace characters
    
    System.out.println("Number of non-whitespace characters: " + nonWSChar);
    printer = printMenu(); // prints menu again
    continue;
    }
    else if (printer == 'w'){ // count number of words
    int numWords = getNumOfWords(st);
    System.out.println("Number of words: " + numWords);
    printer = printMenu();
    continue;
    }
    else if (printer == 'f'){ // find word
    System.out.println("Enter a word or phrase to be found:");
    String findText = myScanner.nextLine(); 
    int numText = findText(findText, st);
    System.out.println("'" + findText + "' instances: " + numText);
    printer = printMenu();
    continue;
    }
    else if (printer == 'r'){ // replace '!' with '.'
    String fixText = replaceExclamation(st);
    System.out.println("Edited text: " + fixText);
    printer = printMenu();
    continue;
    }
    else if (printer == 's'){ // shorten length of 2 or more spaces
    String fixSpace = shortenSpace(st);
    System.out.println("Edited text: " + fixSpace);
    printer = printMenu();
    continue;
    }
    else{ // path taken if an invalid input is typed by the user and asks for a valid one
    System.out.println("");
    
    System.out.println("Please type a valid input.");
    printer = printMenu();
    continue;
}
    }
}



