//Michaela Lewis
//Hw #4 Pt 2
//This program plays a dice (random or user input), determines the slang terminology 
//and prints it using switch statements 

import java.util.Scanner;

public class CrapsSwitch{
  public static void main (String[]args){
    
    //asks the user if they would like to randomly cast a dice
    Scanner userScanner = new Scanner (System.in);
    System.out.println("Would you like to randomly cast dice enter 1 for YES and 2 for NO");
    int responce = userScanner.nextInt();
    
    //variables
    int dice1;
    int dice2;
    int userDice;
    int userDice2;
    //determine the slang terminology of the outcome of the roll 
    int totalRanDice;//randomly selected dice
    int totalUserInp;//user inputed dice;
    String slangTerm;
    
    //switch statements
    //yes 
    switch(responce) 
   {
    case 1: //yes
     dice1 = (int) (Math.random()*6)+1;
     dice2 = (int) (Math.random()*6)+1;
     System.out.println("Your dice are: " + dice1 + " and " + dice2);
        
        
    //what is the roll called
      switch(dice1)
        {
          case 1: //dice 1 is 1
            switch(dice2)
            {
              case 1: slangTerm = "Snake Eyes";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 2: slangTerm = "Ace Deuce";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 3: slangTerm = "Easy Four";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 4: slangTerm = "Fever Five";
                      System.out.println("You rolled: " + slangTerm);
              break;  
              case 5: slangTerm = "Easy Six";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 6: slangTerm = "Seven Out";
                      System.out.println("You rolled: " + slangTerm);
              break;
            } //end of dice1 being 
          break;
          
          case 2: //userDice is two
            switch(dice2)
            {
             
              case 2: slangTerm = "Hard Four";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 3: slangTerm = "Fever Five";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 4: slangTerm = "Easy Six";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 5: slangTerm = "Seven Out";
                      System.out.println("You rolled: " + slangTerm);
              break;  
              case 6: slangTerm = "Easy Eight";
                      System.out.println("You rolled: " + slangTerm);
              break;
            } //end of dice1 being 2
            break;
          
          case 3: //userDice is 3 
            switch(dice2)
            {
              case 3: slangTerm = "Hard Six";
                        System.out.println("You rolled: " + slangTerm);
              break;
              case 4: slangTerm = "Seven Out";
                        System.out.println("You rolled: " + slangTerm);
              break;
              case 5: slangTerm = "Easy Eight";
                        System.out.println("You rolled: " + slangTerm);
              break;
              case 6: slangTerm = "Nine";
                        System.out.println("You rolled: " + slangTerm);
              break;
            }
          break;
          
        case 4: //userDice is 4
          switch(dice2)
          {
                case 4: slangTerm = "Hard Eight";
                          System.out.println("You rolled: " + slangTerm);
                break;
                case 5: slangTerm = "Nine";
                          System.out.println("You rolled: " + slangTerm);
                break;
                case 6: slangTerm = "Easy Ten";
                          System.out.println("You rolled: " + slangTerm);
                break;
          }
          break;
          
        case 5: //userDice is 5
            switch(dice2)
            {
                case 5: slangTerm = "Hard Ten";
                          System.out.println("You rolled: " + slangTerm);
                break;
                case 6: slangTerm = "Yo-leven";
                          System.out.println("You rolled: " + slangTerm);
                break;
            }
           break;
          
        case 6: //userDice is 6
          switch(dice2)
          {
                case 6: slangTerm = "Hard Six";
                    System.out.println("You rolled: " + slangTerm);
                break;
          }
         
          
           
      }

          break; //end of yes 
         
            
    case 2: //no -> input dice myself 
      Scanner myScanner1 = new Scanner (System.in);
      System.out.println("Please enter a value for your first dice:");
      userDice = myScanner1.nextInt();

      Scanner myScanner2 = new Scanner (System.in);
      System.out.println("Please enter a value for your second dice:");
      userDice2 = myScanner2.nextInt();
         if ((userDice < 1 || userDice > 6) || (userDice2 < 1 || userDice2 > 6))
    {
    System.out.println("ERROR! Please enter a value for dice that is between 1 and 6 {inclusive}");
    }
    else
    {
      
      switch (userDice)
      {
          case 1: // userDice 1 is one
            switch (userDice2)
            {
              case 1: slangTerm = "Snake Eyes";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 2: slangTerm = "Ace Deuce";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 3: slangTerm = "Easy Four";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 4: slangTerm = "Fever Five";
                      System.out.println("You rolled: " + slangTerm);
              break;  
              case 5: slangTerm = "Easy Six";
                      System.out.println("You rolled: " + slangTerm);
              break;
              case 6: slangTerm = "Seven Out";
                      System.out.println("You rolled: " + slangTerm);
              break;
            } //end of userDice being 1
          break;
          
         case 2: //userDice is two
          switch(userDice2)
          {
            case 2: slangTerm = "Hard Four";
                    System.out.println("You rolled: " + slangTerm);
            break;
            case 3: slangTerm = "Fever Five";
                    System.out.println("You rolled: " + slangTerm);
            break;
            case 4: slangTerm = "Easy Six";
                    System.out.println("You rolled: " + slangTerm);
            break;
            case 5: slangTerm = "Seven Out";
                        System.out.println("You rolled: " + slangTerm);
            break;  
            case 6: slangTerm = "Easy Eight";
                        System.out.println("You rolled: " + slangTerm);
            break;
          } //end of Userdice being 2
          break;
          
        case 3: //userDice is 3
          switch(userDice2)
          {
          case 3: slangTerm = "Hard Six";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 4: slangTerm = "Seven Out";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 5: slangTerm = "Easy Eight";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 6: slangTerm = "Nine";
                    System.out.println("You rolled: " + slangTerm);
          break;
          } //end of Userdice being 2
        break;
        case 4: //userDice is 4
          switch(userDice2)
          {
          case 4: slangTerm = "Hard Eight";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 5: slangTerm = "Nine";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 6: slangTerm = "Easy Ten";
                    System.out.println("You rolled: " + slangTerm);
          break;
          } 
         break;
        case 5: //userDice is 5
          switch(userDice2)
          {
          case 5: slangTerm = "Hard Ten";
                    System.out.println("You rolled: " + slangTerm);
          break;
          case 6: slangTerm = "Yo-leven";
                    System.out.println("You rolled: " + slangTerm);
          break;
          }
        break;
        case 6: //userDice is 6
          switch(userDice2)
          {
          case 6: slangTerm = "Hard Six";
                    System.out.println("You rolled: " + slangTerm);
          break;
          }
        
          
      }
       break; 
    
    }
    }//responce
      
    
  } //method
}  //class
