//Michaela Lewis
//09/06/2018
//Program records two types of data for the front wheel of a bicycle 
public class Cyclometer {
 
  public static void main(String[] args){
    int secsTrip1 = 480;  //# of seconds of trip 1
    int secsTrip2 = 3220;  //# of seconds of trip 2
		int countsTrip1 = 1561;  // # of rotations of trip 1
		int countsTrip2 = 9037; // # of rotations of trip 2
    double wheelDiameter = 27.0;  //wheel diameter
  	double PI = 3.14159; // pi
  	int feetPerMile = 5280;  // feet per mile
  	int inchesPerFoot = 12;   // inches per feet
  	int secondsPerMinute = 60;  //seconds per min
  	double distanceTrip1, distanceTrip2,totalDistance;  //distances
    
    //outputs and calculations 
        System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
    //distance computations
    //distance in inches
  distanceTrip1 = (countsTrip1*wheelDiameter*PI);
    
    // Gives distance in miles for trips 1 and 2
	distanceTrip1 /= inchesPerFoot*feetPerMile;
	distanceTrip2 = (double) (countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile);
	totalDistance = distanceTrip1+distanceTrip2;
    
    //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

    

  } //method end 
} //class end