//Michaela Lewis
//This program takes user inputs about a class but only accepts the requested data type


import java.util.Scanner;

public class loopPrac{
    public static void main(String args[]) {
        
      Scanner myScanner = new Scanner(System.in);
       
      //course number
        System.out.println("enter your course number");
      
        while (!myScanner.hasNextInt()){
            String junkword = myScanner.next();
            System.out.println("you can only enter an integer");
        }
        int courseNum = myScanner.nextInt(); 
        
      //department name
        System.out.println("enter the department name");
        while (myScanner.hasNextInt()){
          String junkword = myScanner.next();
          System.out.println("Error, please enter a name:");
          
        }
      
       String deptNam = myScanner.nextLine();
      
     //# of times met in a week
      System.out.println("enter the number of times it meets in a week");
        while (!myScanner.hasNextInt()){
            String junkword = myScanner.next();
            System.out.println("you can only enter an integer");
        }
        int meetNum = myScanner.nextInt(); 
      
     // time the class starts
      System.out.println("enter the time the class starts");
        while (!myScanner.hasNextInt()){
            String junkword = myScanner.next();
            System.out.println("you can only enter an integer");
        }
        int classTime = myScanner.nextInt();
      
      //instructor name 
       System.out.println("enter the instructor name");
        while (myScanner.hasNextInt()){
          String junkword = myScanner.next();
          System.out.println("Error, please enter a name:");
        }
        String instrName = myScanner.nextLine();
      
      //# of students
       System.out.println("enter the number of students in your class");
        while (!myScanner.hasNextInt()){
            String junkword = myScanner.next();
            System.out.println("you can only enter an integer");
        }
        int numStudents = myScanner.nextInt();
        
      
      //print everything
      System.out.println("Your course number is: "+ courseNum);
      System.out.println("The department is: "+ deptNam);
      System.out.println("The number of times the class meets is: "+ meetNum);
      System.out.println("The class starts at: "+ classTime);
      System.out.println("The instructor name is: "+ instrName);
      System.out.println("The number of students is: "+ numStudents);
      
      
    }
}