import java.util.Random;
import java.util.Scanner;


public class methodPrac{

 //adjective method
public static String Adjectives() {

Random randomGenerator = new Random();
int adjInt = randomGenerator.nextInt(10);
String adjective = "some ad";
switch (adjInt) {
    case 0: 
    adjective = "grumpy";
    break;
  
    case 1: 
    adjective = "kind ";
    break;

    case 2: 
    adjective = "sweet "; 
    break;

    case 3: 
    adjective = "cold "; 
    break;

    case 4: 
    adjective = "warm "; 
    break;

    case 5: 
    adjective = "rowdy "; 
    break;

    case 6: 
    adjective = "small "; 
    break;

    case 7: 
    adjective = "tall "; 
    break;

    case 8: 
    adjective = "round "; 
    break;

    case 9: 
    adjective = "square "; 
    break;
    
    
}

return adjective;


}

//non primary noun method for subject
public static String Subject() {

Random randomGenerator = new Random();
int subInt= randomGenerator.nextInt(10);
String subject = "some ad";
switch (subInt) {
    case 0: 
    subject = "boy ";
    break;
  
    case 1: 
    subject = "tree ";
    break;

    case 2: 
    subject = "lady "; 
    break;

    case 3: 
    subject = "grandmother "; 
    break;

    case 4: 
    subject = "sky "; 
    break;

    case 5: 
    subject = "mascot "; 
    break;

    case 6: 
    subject = "animal "; 
    break;

    case 7: 
    subject = "girl "; 
    break;

    case 8: 
    subject = "squirrel "; 
    break;

    case 9: 
    subject = "grandfather "; 
    break;
    
    
}

return subject;
}
  
//passed tense verb method
public static String Verb() {

Random randomGenerator = new Random();
int verbInt = randomGenerator.nextInt(10);
String verb = "some ad";
switch (verbInt) {
    case 0: 
    verb = "ran ";
    break;
  
    case 1: 
    verb = "walked ";
    break;

    case 2: 
    verb = "skipped "; 
    break;

    case 3: 
    verb = "jumped "; 
    break;

    case 4: 
    verb = "hopped "; 
    break;

    case 5: 
    verb = "shook "; 
    break;

    case 6: 
    verb = "knocked "; 
    break;

    case 7: 
    verb = "pushed "; 
    break;

    case 8: 
    verb = "passed "; 
    break;

    case 9: 
    verb = "squeezed "; 
    break;
    
    
}

return verb;
}

//Non-primary nouns appropriate for the object method
public static String methObj() {

Random randomGenerator = new Random();
int objInt = randomGenerator.nextInt(10);
String thing = "some ad";
switch (objInt) {
    case 0: 
    thing = "plant matter ";
    break;
  
    case 1: 
    thing = "orange ";
    break;

    case 2: 
    thing = "sponge "; 
    break;

    case 3: 
    thing = "lemonade "; 
    break;

    case 4: 
    thing = "bike "; 
    break;

    case 5: 
    thing = "book "; 
    break;

    case 6: 
    thing = "fork "; 
    break;

    case 7: 
    thing = "chair "; 
    break;

    case 8: 
    thing = "teacher "; 
    break;

    case 9: 
    thing = "door "; 
    break;
    
    
}

return thing;
}
   
//phase 1
public static String Sent1(){
  Scanner myScanner = new Scanner (System.in);
  int userAnswer;
  String senSub1 = "";
  System.out.println("Would you like to generate a sentence? enter 1 for yes and 2 for no");
  userAnswer = myScanner.nextInt();
  do{
  
 //print
// System.out.print("The ");
    String senAdj = Adjectives();
 //System.out.print(metAdj);

    senSub1 = Subject();
 //System.out.print(subject);

   String senVerb = Verb();
 //System.out.println(verb);

// System.out.print("The ");

    String senSub2 = methObj();
 //System.out.print(thing);
    System.out.println("The " + senAdj + senSub1 + senVerb + "the " + senSub2);
    System.out.println("Would you like to print another sentence?");
    userAnswer = myScanner.nextInt();
    if(userAnswer == 1){
      continue;
    }
    else{
      break;
    }
   }while (userAnswer == 1);
  
 return senSub1; 
}



//public static String actSen(){
 // Sent1();
  
	/*int somenum = (int) (Math.random()*2);
  if (somenum == 1){
    System.out.print("It");
  }
  else{
   //  String refSen1Sub = senSub1;
   // System.out.print();
   */
 // }
  
//}


 public static void main(String[]args){
 
   Sent1();
 
 }
}

   
   