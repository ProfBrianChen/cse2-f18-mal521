import java.util.Scanner;

//Michaela Lewis
//The program uses while loops to calculate the probability of poker hands 

public class Hw05{
  public static void main (String[]args){
    
    Scanner myScanner = new Scanner(System.in);
    System.out.println("how many hands do you want generated: ");
    int handGen = myScanner.nextInt();
    
    // for each hand generate 5 random number from 1 to 52 representing the four sets 
    //cards 
    int counter = 0;
    
    
    int card1, card2, card3, card4, card5;
    //counters
    int aceCounter = 0;
    int twoCounter = 0;
    int threeCounter = 0;
    int fourCounter = 0;
    int fiveCounter = 0;
    int sixCounter = 0;
    int sevenCounter = 0;
    int eightCounter = 0;
    int nineCounter = 0;
    int tenCounter = 0;
    int jackCounter = 0;
    int queenCounter = 0;
    int kingCounter = 0;
    double tallyOnePair = 0;
    double tallyThreeAKind = 0;
    double tallyFourAKind = 0;
    double tallyTwoPair = 0;
    double numberOnePair = 0;
    double numberTwoPair = 0;
    
    
    //generate cards 
    while (counter < handGen){
      card1 = (int)(Math.random()*52)+1;
      card2 = (int)(Math.random()*52)+1;
      card3 = (int)(Math.random()*52)+1;
      card4 = (int)(Math.random()*52)+1;
      card5 = (int)(Math.random()*52)+1;
      
    //separate
      card1 = card1 % 13;
      card2 = card2 % 13;
      card3 = card3 % 13;
      card4 = card4 % 13;
      card5 = card5 % 13;
 
      if (card1==1){
        ++aceCounter;
      }
      else if(card1==2){
        ++twoCounter;
      }
      else if(card1==3){
        ++threeCounter;
      }
      else if(card1==4){
        ++fourCounter;
      }
      else if(card1==5){
        ++fiveCounter;
      }
      else if(card1==6){
        ++sixCounter;
      }
      else if(card1==7){
        ++sevenCounter;
      }
      else if(card1==8){
        ++eightCounter;
      }
      else if(card1==9){
        ++nineCounter;
      }
      else if(card1==10){
        ++tenCounter;
      }
      else if(card1==11){
        ++jackCounter;
      }
      else if(card1==12){
        ++queenCounter;
      }
      else if(card1==0){
        ++kingCounter;
      }
      
      
      if (card2==1){
        ++aceCounter;
      }
      else if(card2==2){
        ++twoCounter;
      }
      else if(card2==3){
        ++threeCounter;
      }
      else if(card2==4){
        ++fourCounter;
      }
      else if(card2==5){
        ++fiveCounter;
      }
      else if(card2==6){
        ++sixCounter;
      }
      else if(card2==7){
        ++sevenCounter;
      }
      else if(card2==8){
        ++eightCounter;
      }
      else if(card2==9){
        ++nineCounter;
      }
      else if(card2==10){
        ++tenCounter;
      }
      else if(card2==11){
        ++jackCounter;
      }
      else if(card2==12){
        ++queenCounter;
      }
      else if(card2==0){
        ++kingCounter;
      }
      
      if (card3==1){
        ++aceCounter;
      }
      else if(card3==2){
        ++twoCounter;
      }
      else if(card3==3){
        ++threeCounter;
      }
      else if(card3==4){
        ++fourCounter;
      }
      else if(card3==5){
        ++fiveCounter;
      }
      else if(card3==6){
        ++sixCounter;
      }
      else if(card3==7){
        ++sevenCounter;
      }
      else if(card3==8){
        ++eightCounter;
      }
      else if(card3==9){
        ++nineCounter;
      }
      else if(card3==10){
        ++tenCounter;
      }
      else if(card3==11){
        ++jackCounter;
      }
      else if(card3==12){
        ++queenCounter;
      }
      else if(card3==0){
        ++kingCounter;
      }
      
      if (card4==1){
        ++aceCounter;
      }
      else if(card4==2){
        ++twoCounter;
      }
      else if(card4==3){
        ++threeCounter;
      }
      else if(card4==4){
        ++fourCounter;
      }
      else if(card4==5){
        ++fiveCounter;
      }
      else if(card4==6){
        ++sixCounter;
      }
      else if(card4==7){
        ++sevenCounter;
      }
      else if(card4==8){
        ++eightCounter;
      }
      else if(card4==9){
        ++nineCounter;
      }
      else if(card4==10){
        ++tenCounter;
      }
      else if(card4==11){
        ++jackCounter;
      }
      else if(card4==12){
        ++queenCounter;
      }
      else if(card4==0){
        ++kingCounter;
      }
      
      if (card5==1){
        ++aceCounter;
      }
      else if(card5==2){
        ++twoCounter;
      }
      else if(card5==3){
        ++threeCounter;
      }
      else if(card5==4){
        ++fourCounter;
      }
      else if(card5==5){
        ++fiveCounter;
      }
      else if(card5==6){
        ++sixCounter;
      }
      else if(card5==7){
        ++sevenCounter;
      }
      else if(card5==8){
        ++eightCounter;
      }
      else if(card5==9){
        ++nineCounter;
      }
      else if(card5==10){
        ++tenCounter;
      }
      else if(card5==11){
        ++jackCounter;
      }
      else if(card5==12){
        ++queenCounter;
      }
      else if(card5==0){
        ++kingCounter;
      }
      
      if (aceCounter == 2){
          tallyTwoPair += 1;
      }
      if (twoCounter == 2){
          tallyTwoPair += 1;
      }
      if (threeCounter == 2){
          tallyTwoPair += 1;
      }
      if (fourCounter == 2){
          tallyTwoPair += 1;
      }
      if (fiveCounter == 2){
          tallyTwoPair += 1;
      }
      if (sixCounter == 2){
          tallyTwoPair += 1;
      }
      if (sevenCounter == 2){
          tallyTwoPair += 1;
      }
      if (eightCounter == 2){
          tallyTwoPair += 1;
      }
      if (nineCounter == 2){
          tallyTwoPair += 1;
      }
      if (tenCounter == 2){
          tallyTwoPair += 1;
      }
      if (jackCounter == 2){
          tallyTwoPair += 1;
      }
      if (queenCounter == 2){
          tallyTwoPair += 1;
      }
      if (kingCounter == 2){
          tallyTwoPair += 1;
      }
      
      if (tallyTwoPair == 2){
          numberTwoPair += 1;
      }
      else if (tallyOnePair == 1){
          numberOnePair += 1;
      }
      
      if (aceCounter == 4 || twoCounter == 4 || threeCounter == 4 || fourCounter == 4 || fiveCounter == 4 || sixCounter == 4 || sevenCounter == 4 || eightCounter == 4 || nineCounter == 4|| tenCounter == 4|| jackCounter == 4 || queenCounter == 4 || kingCounter == 4){
       tallyFourAKind += 1;   
      }
      
      if (aceCounter == 3 || twoCounter == 3 || threeCounter == 3 || fourCounter == 3 || fiveCounter == 3 || sixCounter == 3 || sevenCounter == 3 || eightCounter == 3 || nineCounter == 3|| tenCounter == 3|| jackCounter == 3 || queenCounter == 3 || kingCounter == 3){
       tallyThreeAKind += 1;   
      }
      
      aceCounter = 0;
      twoCounter = 0;
      threeCounter = 0;
      fourCounter = 0;
      fiveCounter = 0;
      sixCounter = 0;
      sevenCounter = 0;
      eightCounter = 0;
      nineCounter = 0;
      tenCounter = 0;
      jackCounter = 0;
      queenCounter = 0;
      kingCounter = 0;
      
      counter += 1;
    }
    
    double fourProp = (tallyFourAKind / handGen);
    double threeProp = tallyThreeAKind / handGen;
    double onePairProp = numberOnePair / handGen;
    double twoPairProp = numberTwoPair / handGen;
    
   //probability
 String strDouble = String.format("%.3f", fourProp);
 String strDouble1 = String.format("%.3f", threeProp);
 String strDouble2 = String.format("%.3f", twoPairProp);
 String strDouble3 = String.format("%.3f", onePairProp);
 
    System.out.println("The probability of four same face cards in a hand is " + strDouble);
    System.out.println("The probability of three same face cards in a hand is " + strDouble1);
    System.out.println("The probability of two pairs in a hand is " + strDouble2);
    System.out.println("The probability of one pair in a hand is " + strDouble3);


    } 
  
    }
    
 

      
      